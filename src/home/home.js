import React from 'react';

import BackgroundImg from '../commons/images/future-medicine.jpg';

import {Button, Container, Jumbotron, Nav} from 'reactstrap';
import JsonRpcClient from "react-jsonrpc-client";
import {HOST} from "../commons/hosts";
import Table from "../commons/tables/table";

const backgroundStyle = {
    backgroundPosition: 'center',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    width: "100%",
    height: "1920px",
    backgroundImage: `url(${BackgroundImg})`
};
const textStyle = {color: 'white', };

const PATIENT_ID = 3;
const RPC_MEDICATION_PLAN = "sendMedicationPlan";
const RPC_PILL_TAKEN = "pillTaken";
const RPC_PILL_NOT_TAKEN = "pillNotTaken";
const DEFAULT_INTAKE_INTERVAL = 90; // 90 seconds

class Home extends React.Component {

    constructor(props) {
        super(props);
        this.rpcClient = new JsonRpcClient({
            endpoint : HOST.backend_api + '/medAppRpc'
        });
        this.state = {
            medicationPlans : null,
            downloadSet : false,
            isDataLoaded: false,
            populatedDataIsSet: false,
            tableData : []
        }
    }

    // mai jos in
    // https://stackoverflow.com/questions/3733227/javascript-seconds-to-minutes-and-seconds
    fancyTimeFormat(duration)
    {
        // Hours, minutes and seconds
        var hrs = ~~(duration / 3600);
        var mins = ~~((duration % 3600) / 60);
        var secs = ~~duration % 60;

        // Output like "1:01" or "4:03:59" or "123:03:59"
        var ret = "";

        if (hrs > 0) {
            ret += "" + hrs + ":" + (mins < 10 ? "0" : "");
        }

        ret += "" + mins + ":" + (secs < 10 ? "0" : "");
        ret += "" + secs;
        return ret;
    }

    //https://stackoverflow.com/questions/23593052/format-javascript-date-as-yyyy-mm-dd
    formatDate(date) {
        var d = date,
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();

        if (month.length < 2)
            month = '0' + month;
        if (day.length < 2)
            day = '0' + day;

        return [day, month, year].join('/') +' '+ date.getHours()+ ':' + date.getMinutes();
    }

    //Downloads the medication plan every day at 00:00 GMT + 2
    scheduleDownload(){
        if(this.state.downloadSet === false){
            this.notificationTimer = setInterval( () => {
                const currentTime = new Date();
                if(currentTime.getHours() === 0 && currentTime.getMinutes() === 0){
                    this.rpcClient.request(RPC_MEDICATION_PLAN, PATIENT_ID).then((response) => {

                        this.setState({'medicationPlans' : response, 'isDataLoaded' : true});
                        this.populateTableData();
                    });
                }
                console.log("Daily update function called at " + currentTime.getHours()+ ':' + currentTime.getMinutes());


            },60000); // 1 minute
            this.setState({'downloadSet' : true});
        }
    }

    forceMedicationPlanUpdate(){
        this.rpcClient.request(RPC_MEDICATION_PLAN, PATIENT_ID).then((response) => {
            this.setState({'medicationPlans' : response, 'isDataLoaded' : true});
            console.log('dataLoaded');
            this.populateTableData();
            //console.log(response);
        });
    }

    pillTaken(medication){
        clearInterval(medication.countdownTimer);
        this.rpcClient.request(RPC_PILL_TAKEN, PATIENT_ID, medication.drug.drugName, this.formatDate(new Date())).then((response) => {

            let data = this.state.tableData;

            const idx = data.indexOf(medication);
            data.splice(idx, 1);

            this.setState({"tableData":data});
        });


    }

    pillNotTaken(medication){

        clearInterval(medication.countdownTimer);

        this.rpcClient.request(RPC_PILL_NOT_TAKEN, PATIENT_ID, medication.drug.drugName, this.formatDate(new Date())).then((response) => {
            let data = this.state.tableData;
            const idx = data.indexOf(medication);
            data.splice(idx, 1);
            this.setState({"tableData":data});
        });
    }

    countdownHandler(tableElement){
        let data = this.state.tableData;
        const idx = data.indexOf(tableElement);

        if(tableElement.counter <= 0){
            this.pillNotTaken(tableElement);
        }else{
            tableElement.counter--;
        }

        data[idx] = tableElement;
        this.setState({"tableData":data});
    }

    populateTableData(){
        if(this.state.populatedDataIsSet === false){
            //if(this.state.isDataLoaded){

                const populateInterval = setInterval(() =>{
                    let clearedMedicationPlans = this.state.medicationPlans.filter(m =>(m.medicationPlanId !== undefined));
                    clearedMedicationPlans.forEach((okPlan) => {
                        okPlan.prescribedMedications = okPlan.prescribedMedications.filter( med => (med.prescribedMedicationId !== undefined));
                    });
                    //console.log(clearedMedicationPlans);
                    let data = this.state.tableData;
                    clearedMedicationPlans.forEach((okPlan) => {

                        okPlan.prescribedMedications.forEach((elm) =>{
                            let copy = JSON.parse(JSON.stringify(elm));
                            copy.takenButton = <Button onClick={() => { this.pillTaken(copy) }}>Take Pill</Button>
                            copy.counter = DEFAULT_INTAKE_INTERVAL + Math.floor(Math.random() * Math.floor(30));
                            copy.countdownTimer = setInterval(() => {
                                this.countdownHandler(copy);
                            }, 1000);

                            data.push(copy);
                            return;
                        });
                    });
                    this.setState({"tableData" : data});
                    // console.log("fac ceva");
                },60000); // 1 minute

                this.setState({'populatedDataIsSet': true});
           // }
        }
    }

    componentDidMount() {
        this.scheduleDownload();
        //this.populateTableData();
    }


    render() {

        if(this.state.isDataLoaded){
          //  this.populateTableData();
        }
       // console.log(this.tableData);

        return (


            <div>
                <Jumbotron fluid style={backgroundStyle}>
                    <Container  fluid>
                        <h1 className="display-3" style={textStyle}>Pill dispenser</h1>
                        <Button className='forceButton' onClick={()=>{this.forceMedicationPlanUpdate();}}>DownloadPlan</Button>
                        <br/>
                        <div className='container-fluid tableClass'>
                            <div className='tableDiv col-sm-6'>
                                <table className='table table-striped '>
                                    <thead>
                                    <tr>
                                        <th>Button</th>
                                        <th>Medicine</th>
                                        <th>Dosage</th>
                                        <th>Timer</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    {this.state.tableData.map(elm =>{
                                        return <tr>
                                            <td>{elm.takenButton}</td>
                                            <td>{elm.drug.drugName} </td>
                                            <td>{elm.dosage}</td>
                                            <td>{this.fancyTimeFormat(elm.counter)}</td>
                                        </tr>
                                    })}
                                    </tbody>
                                </table>
                            </div>

                        </div>


                    </Container>
                </Jumbotron>

            </div>
        )
    };
}

export default Home
