# OpenApiDefinition.UserBodyDTOs

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**userDTO** | [**UserDTO**](UserDTO.md) |  | [optional] 
**userDetailsDTO** | [**UserDetailsDTO**](UserDetailsDTO.md) |  | [optional] 


