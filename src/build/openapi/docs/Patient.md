# OpenApiDefinition.Patient

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**patientId** | **Number** |  | [optional] 
**user** | [**User**](User.md) |  | [optional] 
**doctor** | [**Doctor**](Doctor.md) |  | [optional] 
**caregiver** | [**Caregiver**](Caregiver.md) |  | [optional] 
**medicationPlans** | [**[MedicationPlan]**](MedicationPlan.md) |  | [optional] 
**medicalRecord** | [**[Illness]**](Illness.md) |  | [optional] 


