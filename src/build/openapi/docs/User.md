# OpenApiDefinition.User

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**userId** | **Number** |  | [optional] 
**userName** | **String** |  | [optional] 
**userPassword** | **String** |  | [optional] 
**userFirstName** | **String** |  | [optional] 
**userLastName** | **String** |  | [optional] 
**userBirthDate** | **Date** |  | [optional] 
**userAddress** | **String** |  | [optional] 
**userGender** | **String** |  | [optional] 
**userRole** | **String** |  | [optional] 



## Enum: UserRoleEnum


* `PATIENT` (value: `"PATIENT"`)

* `DOCTOR` (value: `"DOCTOR"`)

* `CAREGIVER` (value: `"CAREGIVER"`)




