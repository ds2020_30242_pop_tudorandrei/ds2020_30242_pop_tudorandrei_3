# OpenApiDefinition.UserDTO

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** |  | [optional] 
**userName** | **String** |  | [optional] 
**password** | **String** |  | [optional] 
**role** | **String** |  | [optional] 
**links** | [**[Link]**](Link.md) |  | [optional] 



## Enum: RoleEnum


* `PATIENT` (value: `"PATIENT"`)

* `DOCTOR` (value: `"DOCTOR"`)

* `CAREGIVER` (value: `"CAREGIVER"`)




