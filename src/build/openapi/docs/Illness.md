# OpenApiDefinition.Illness

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**illnessId** | **Number** |  | [optional] 
**name** | **String** |  | [optional] 
**chronic** | **Boolean** |  | [optional] 
**affectedPatients** | [**[Patient]**](Patient.md) |  | [optional] 


