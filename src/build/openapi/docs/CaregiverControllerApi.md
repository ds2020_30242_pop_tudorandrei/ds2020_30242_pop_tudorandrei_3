# OpenApiDefinition.CaregiverControllerApi

All URIs are relative to *https://spring-demo-ds2020-pls-work.herokuapp.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**findAssignedPatientsById**](CaregiverControllerApi.md#findAssignedPatientsById) | **GET** /caregiver/assignedPatients/{caregiverId} | 
[**findById**](CaregiverControllerApi.md#findById) | **GET** /caregiver/findById/{caregiverId} | 
[**getCaregivers**](CaregiverControllerApi.md#getCaregivers) | **GET** /caregiver/getAll | 



## findAssignedPatientsById

> [Patient] findAssignedPatientsById(caregiverId)



### Example

```javascript
import OpenApiDefinition from 'open_api_definition';

let apiInstance = new OpenApiDefinition.CaregiverControllerApi();
let caregiverId = 789; // Number | 
apiInstance.findAssignedPatientsById(caregiverId, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **caregiverId** | **Number**|  | 

### Return type

[**[Patient]**](Patient.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*


## findById

> Caregiver findById(caregiverId)



### Example

```javascript
import OpenApiDefinition from 'open_api_definition';

let apiInstance = new OpenApiDefinition.CaregiverControllerApi();
let caregiverId = 789; // Number | 
apiInstance.findById(caregiverId, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **caregiverId** | **Number**|  | 

### Return type

[**Caregiver**](Caregiver.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*


## getCaregivers

> [Caregiver] getCaregivers()



### Example

```javascript
import OpenApiDefinition from 'open_api_definition';

let apiInstance = new OpenApiDefinition.CaregiverControllerApi();
apiInstance.getCaregivers((error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**[Caregiver]**](Caregiver.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

