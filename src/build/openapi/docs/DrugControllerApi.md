# OpenApiDefinition.DrugControllerApi

All URIs are relative to *https://spring-demo-ds2020-pls-work.herokuapp.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getAllDrugs**](DrugControllerApi.md#getAllDrugs) | **GET** /drug/getAllDrugs | 
[**getDrugById**](DrugControllerApi.md#getDrugById) | **GET** /drug/getById/{drugId} | 



## getAllDrugs

> [Drug] getAllDrugs()



### Example

```javascript
import OpenApiDefinition from 'open_api_definition';

let apiInstance = new OpenApiDefinition.DrugControllerApi();
apiInstance.getAllDrugs((error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**[Drug]**](Drug.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*


## getDrugById

> Drug getDrugById(drugId)



### Example

```javascript
import OpenApiDefinition from 'open_api_definition';

let apiInstance = new OpenApiDefinition.DrugControllerApi();
let drugId = 789; // Number | 
apiInstance.getDrugById(drugId, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **drugId** | **Number**|  | 

### Return type

[**Drug**](Drug.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

