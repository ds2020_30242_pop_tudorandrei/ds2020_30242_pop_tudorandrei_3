# OpenApiDefinition.UserControllerApi

All URIs are relative to *https://spring-demo-ds2020-pls-work.herokuapp.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**addUser**](UserControllerApi.md#addUser) | **POST** /user/create | 
[**deleteUser**](UserControllerApi.md#deleteUser) | **POST** /deleteUser/{id} | 
[**getAllUsers**](UserControllerApi.md#getAllUsers) | **GET** /all/users | 
[**logIn**](UserControllerApi.md#logIn) | **POST** /user/logIn | 
[**updateUser**](UserControllerApi.md#updateUser) | **POST** /updateUsername/{id}/{newUsername} | 



## addUser

> User addUser(opts)



### Example

```javascript
import OpenApiDefinition from 'open_api_definition';

let apiInstance = new OpenApiDefinition.UserControllerApi();
let opts = {
  'userBodyDTOs': new OpenApiDefinition.UserBodyDTOs() // UserBodyDTOs | 
};
apiInstance.addUser(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userBodyDTOs** | [**UserBodyDTOs**](UserBodyDTOs.md)|  | [optional] 

### Return type

[**User**](User.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: */*


## deleteUser

> deleteUser(id)



### Example

```javascript
import OpenApiDefinition from 'open_api_definition';

let apiInstance = new OpenApiDefinition.UserControllerApi();
let id = 789; // Number | 
apiInstance.deleteUser(id, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**|  | 

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined


## getAllUsers

> [User] getAllUsers()



### Example

```javascript
import OpenApiDefinition from 'open_api_definition';

let apiInstance = new OpenApiDefinition.UserControllerApi();
apiInstance.getAllUsers((error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**[User]**](User.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*


## logIn

> User logIn(opts)



### Example

```javascript
import OpenApiDefinition from 'open_api_definition';

let apiInstance = new OpenApiDefinition.UserControllerApi();
let opts = {
  'logInEntity': new OpenApiDefinition.LogInEntity() // LogInEntity | 
};
apiInstance.logIn(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **logInEntity** | [**LogInEntity**](LogInEntity.md)|  | [optional] 

### Return type

[**User**](User.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: */*


## updateUser

> updateUser(id, newUsername)



### Example

```javascript
import OpenApiDefinition from 'open_api_definition';

let apiInstance = new OpenApiDefinition.UserControllerApi();
let id = 789; // Number | 
let newUsername = "newUsername_example"; // String | 
apiInstance.updateUser(id, newUsername, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
});
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**|  | 
 **newUsername** | **String**|  | 

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

